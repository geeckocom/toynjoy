import Phaser, { Game } from 'phaser';
import { WIDTH, HEIGHT } from './constants/config';
import Init from './scenes/Init';
import Town from './scenes/Town';
import Town_2 from './scenes/Town-2';
import House_1 from './scenes/House-1';
import House_2 from './scenes/House-2';
import RexUIPlugin from 'phaser3-rex-plugins/templates/ui/ui-plugin.js';

const config = {
    type: Phaser.AUTO,
    width: WIDTH,
    height: HEIGHT,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 0 },
            debug: false,
        },
    },
    plugins: {
        scene: [{
            key: 'rexUI',
            plugin: RexUIPlugin,
            mapping: 'rexUI'
        },
            // ...
        ]
    },
    scene: [Init, Town, House_1, House_2, Town_2],
};

const game = new Game(config);
